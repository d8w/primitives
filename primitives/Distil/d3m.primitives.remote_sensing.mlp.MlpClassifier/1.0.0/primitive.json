{
    "algorithm_types": [
        "MULTILAYER_PERCEPTRON"
    ],
    "can_use_gpus": true,
    "description": "This primitive trains a two-layer neural network classifier on featurized remote sensing imagery.\nIt also produces heatmap visualizations for predictions using the gradient-based GradCam technique:\nhttps://arxiv.org/pdf/1610.02391v1.pdf.\n\nTraining inputs: 1) Feature dataframe, 2) Label dataframe\nOutputs: D3M dataset with predictions",
    "digest": "f40e2014b887ff2382e9b9d8909c076907eb1e3adb4941e1548d1225bf45436a",
    "id": "dce5255d-b63c-4601-8ace-d63b42d6d03e",
    "installation": [
        {
            "package": "cython",
            "type": "PIP",
            "version": "0.29.24"
        },
        {
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives.git@49acb225bb6994d3dfaffdf3b7761395423680a4#egg=kf-d3m-primitives",
            "type": "PIP"
        },
        {
            "package": "zlib1g-dev",
            "type": "UBUNTU",
            "version": "1:1.2.11.dfsg-0ubuntu2"
        },
        {
            "package": "liblzo2-dev",
            "type": "UBUNTU",
            "version": "2.08-1.2"
        },
        {
            "package": "python-lzo",
            "type": "PIP",
            "version": "1.12"
        }
    ],
    "keywords": [
        "remote sensing",
        "neural network",
        "classification",
        "explainability",
        "GradCam",
        "GuidedBackProp",
        "GradCam-GuidedBackProp"
    ],
    "name": "MlpClassifier",
    "original_python_path": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.MlpClassifierPrimitive",
    "primitive_code": {
        "arguments": {
            "hyperparams": {
                "kind": "RUNTIME",
                "type": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.Hyperparams"
            },
            "inputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "iterations": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, int]"
            },
            "outputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "params": {
                "kind": "RUNTIME",
                "type": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.Params"
            },
            "produce_methods": {
                "kind": "RUNTIME",
                "type": "typing.Sequence[str]"
            },
            "random_seed": {
                "default": 0,
                "kind": "RUNTIME",
                "type": "int"
            },
            "timeout": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, float]"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "class_methods": {},
        "class_type_arguments": {
            "Hyperparams": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.Hyperparams",
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.Params"
        },
        "hyperparams": {
            "all_confidences": {
                "default": true,
                "description": "whether to return explanations for all classes and all confidences from produce method",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "batch_size": {
                "default": 256,
                "description": "training and inference batch size",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 512,
                "upper_inclusive": true
            },
            "epochs": {
                "default": 25,
                "description": "how many epochs for which to finetune classification head (happens first)",
                "lower": 0,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 9223372036854775807,
                "upper_inclusive": false
            },
            "explain_all_classes": {
                "default": false,
                "description": "whether to return explanations for all classes or only the predicted class",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "feature_dim": {
                "default": 2048,
                "description": "feature dimension after reshaping flattened feature vector",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 9223372036854775807,
                "upper_inclusive": true
            },
            "image_dim": {
                "default": 120,
                "description": "input dimension of image (height and width)",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 512,
                "upper_inclusive": true
            },
            "learning_rate": {
                "default": 0.1,
                "description": "learning rate",
                "lower": 0.0,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "float",
                "type": "d3m.metadata.hyperparams.Uniform",
                "upper": 1.0,
                "upper_inclusive": false
            },
            "weights_filepath": {
                "default": "model_weights.pth",
                "description": "weights of trained model will be saved to this filepath",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "str",
                "type": "d3m.metadata.hyperparams.Hyperparameter"
            }
        },
        "instance_attributes": {
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "temporary_directory": "typing.Union[NoneType, str]",
            "volumes": "typing.Dict[str, str]"
        },
        "instance_methods": {
            "__init__": {
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "fit": {
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "description": "Fits mlp classification head using training data from set_training_data and hyperparameters\n\nKeyword Arguments:\n    timeout {float} -- timeout, considered (default: {None})\n    iterations {int} -- iterations, considered (default: {None})\n\nReturns:\n    CallResult[None]\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]"
            },
            "fit_multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods`` and ``set_training_data``.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "get_params": {
                "arguments": [],
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters.",
                "kind": "OTHER",
                "returns": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.Params"
            },
            "multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "produce": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "Produce primitive's predictions\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``.",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "produce_explanations": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "Produce explanation masks for primitive's predictions\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "set_params": {
                "arguments": [
                    "params"
                ],
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "set_training_data": {
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "description": "Sets primitive's training data\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing features\n    outputs {Outputs} -- D3M dataframe containing targets\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs.",
                "kind": "OTHER",
                "returns": "NoneType"
            }
        },
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "interfaces_version": "2021.11.25.dev0",
        "params": {
            "is_fit": "bool",
            "label_encoder": "sklearn.preprocessing._label.LabelEncoder",
            "nclasses": "int",
            "output_column": "str"
        }
    },
    "primitive_family": "REMOTE_SENSING",
    "python_path": "d3m.primitives.remote_sensing.mlp.MlpClassifier",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "source": {
        "contact": "mailto:cbethune@uncharted.software",
        "name": "Distil",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives"
        ]
    },
    "structural_type": "kf_d3m_primitives.remote_sensing.classifier.mlp_classifier.MlpClassifierPrimitive",
    "version": "1.0.0"
}
