{
    "algorithm_types": [
        "RECURRENT_NEURAL_NETWORK"
    ],
    "can_use_gpus": true,
    "description": "This primitive applies the DeepAR (deep, autoregressive) forecasting methodology for\ntime series prediction. It trains a global model on related time series to produce probabilistic\nforecasts. The implementation is based off of this paper: https://arxiv.org/pdf/1704.04110.pdf\nand this implementation: https://gluon-ts.mxnet.io/index.html",
    "digest": "fe085690a8f9a1d517caab78180af8b92af205042704880d938b5f5452ec4d79",
    "id": "3410d709-0a13-4187-a1cb-159dd24b584b",
    "installation": [
        {
            "package": "cython",
            "type": "PIP",
            "version": "0.29.24"
        },
        {
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives.git@49acb225bb6994d3dfaffdf3b7761395423680a4#egg=kf-d3m-primitives",
            "type": "PIP"
        }
    ],
    "keywords": [
        "time series",
        "forecasting",
        "recurrent neural network",
        "autoregressive"
    ],
    "name": "DeepAR",
    "original_python_path": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.DeepArPrimitive",
    "primitive_code": {
        "arguments": {
            "hyperparams": {
                "kind": "RUNTIME",
                "type": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.Hyperparams"
            },
            "inputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "iterations": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, int]"
            },
            "outputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "params": {
                "kind": "RUNTIME",
                "type": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.Params"
            },
            "produce_methods": {
                "kind": "RUNTIME",
                "type": "typing.Sequence[str]"
            },
            "random_seed": {
                "default": 0,
                "kind": "RUNTIME",
                "type": "int"
            },
            "timeout": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, float]"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "class_methods": {},
        "class_type_arguments": {
            "Hyperparams": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.Hyperparams",
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.Params"
        },
        "hyperparams": {
            "context_length": {
                "default": 30,
                "description": "number of context timesteps to consider before prediction, for both training and test",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 1000,
                "upper_inclusive": true
            },
            "count_data": {
                "configuration": {
                    "auto_selected": {
                        "default": null,
                        "semantic_types": [],
                        "structural_type": "NoneType",
                        "type": "d3m.metadata.hyperparams.Hyperparameter"
                    },
                    "user_selected": {
                        "default": true,
                        "semantic_types": [],
                        "structural_type": "bool",
                        "type": "d3m.metadata.hyperparams.UniformBool"
                    }
                },
                "default": null,
                "description": "Whether we should label the target column as real or count (positive) based on user input or automatic selection. For example, user might want to specify positive only count data if target column is real-valued, but domain is >= 0",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Union[NoneType, bool]",
                "type": "d3m.metadata.hyperparams.Union"
            },
            "dropout_rate": {
                "default": 0.1,
                "description": "dropout to use in lstm model (input and recurrent transform)",
                "lower": 0.0,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "float",
                "type": "d3m.metadata.hyperparams.Uniform",
                "upper": 1.0,
                "upper_inclusive": false
            },
            "epochs": {
                "default": 50,
                "description": "number of training epochs",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 9223372036854775807,
                "upper_inclusive": false
            },
            "inference_batch_size": {
                "default": 256,
                "description": "inference batch size",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 1024,
                "upper_inclusive": true
            },
            "learning_rate": {
                "default": 0.0001,
                "description": "learning rate",
                "lower": 0.0,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "float",
                "type": "d3m.metadata.hyperparams.Uniform",
                "upper": 1.0,
                "upper_inclusive": false
            },
            "lstm_dim": {
                "default": 40,
                "description": "number of cells to use in the lstm component of the model",
                "lower": 10,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 400,
                "upper_inclusive": true
            },
            "nan_padding": {
                "default": true,
                "description": "whether to pad predictions that aren't supported by the model with 'np.nan' or with the last valid prediction",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "num_layers": {
                "default": 2,
                "description": "number of cells to use in the lstm component of the model",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 16,
                "upper_inclusive": true
            },
            "number_samples": {
                "default": 100,
                "description": "number of samples to draw at each timestep from forecast distribution",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 1000,
                "upper_inclusive": true
            },
            "output_mean": {
                "default": true,
                "description": "whether to output mean (or median) forecasts from probability distributions",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "prediction_length": {
                "default": 30,
                "description": "number of future timesteps to predict",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 1000,
                "upper_inclusive": true
            },
            "quantiles": {
                "default": [],
                "description": "A set of quantiles for which to return estimates from forecast distribution",
                "elements": {
                    "default": -1,
                    "semantic_types": [],
                    "structural_type": "float",
                    "type": "d3m.metadata.hyperparams.Hyperparameter"
                },
                "is_configuration": false,
                "min_size": 0,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Sequence[float]",
                "type": "d3m.metadata.hyperparams.Set"
            },
            "steps_per_epoch": {
                "default": 100,
                "description": "number of steps to do per epoch",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 200,
                "upper_inclusive": true
            },
            "training_batch_size": {
                "default": 32,
                "description": "training batch size",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 256,
                "upper_inclusive": true
            },
            "weights_dir": {
                "default": "deepar_weights",
                "description": "weights of trained model will be saved to this filepath",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "str",
                "type": "d3m.metadata.hyperparams.Hyperparameter"
            }
        },
        "instance_attributes": {
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "temporary_directory": "typing.Union[NoneType, str]",
            "volumes": "typing.Dict[str, str]"
        },
        "instance_methods": {
            "__init__": {
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "fit": {
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "description": "Fits DeepAR model using training data from set_training_data and hyperparameters\n\nKeyword Arguments:\n    timeout {float} -- timeout, considered (default: {None})\n    iterations {int} -- iterations, considered (default: {None})\n\nReturns:\n    CallResult[None]\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]"
            },
            "fit_multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods`` and ``set_training_data``.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "get_params": {
                "arguments": [],
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters.",
                "kind": "OTHER",
                "returns": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.Params"
            },
            "multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "produce": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "Produce primitive's predictions for specific time series at specific future time instances\n* these specific timesteps / series are specified implicitly by input dataset\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- (N, 2) dataframe with d3m_index and value for each prediction slice requested.\n        prediction slice = specific horizon idx for specific series in specific regression\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``.",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "produce_confidence_intervals": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "produce quantiles for each prediction timestep in dataframe\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError:\n\nReturns:\n    CallResult[Outputs] --\n\n    Ex.\n        0.50 | 0.05 | 0.95\n        -------------------\n         5   |   3  |   7\n         6   |   4  |   8\n         5   |   3  |   7\n         6   |   4  |   8",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "set_params": {
                "arguments": [
                    "params"
                ],
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "set_training_data": {
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "description": "Sets primitive's training data\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n    outputs {Outputs} -- D3M dataframe containing targets\n\nRaises:\n    ValueError: If multiple columns are annotated with 'Time' or 'DateTime' metadata\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs.",
                "kind": "OTHER",
                "returns": "NoneType"
            }
        },
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "interfaces_version": "2021.11.25.dev0",
        "params": {
            "cat_cols": "typing.List[int]",
            "deepar_dataset": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar_dataset.DeepARDataset",
            "freq": "str",
            "group_cols": "typing.List[int]",
            "is_fit": "bool",
            "min_trains": "typing.Union[typing.Dict[str, pandas._libs.tslibs.timestamps.Timestamp], typing.Dict[typing.Any, pandas._libs.tslibs.timestamps.Timestamp], typing.List[pandas._libs.tslibs.timestamps.Timestamp]]",
            "output_column": "str",
            "real_cols": "typing.List[int]",
            "reind_freq": "str",
            "timestamp_column": "int"
        }
    },
    "primitive_family": "TIME_SERIES_FORECASTING",
    "python_path": "d3m.primitives.time_series_forecasting.lstm.DeepAR",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "source": {
        "contact": "mailto:cbethune@uncharted.software",
        "name": "Distil",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives"
        ]
    },
    "structural_type": "kf_d3m_primitives.ts_forecasting.deep_ar.deepar.DeepArPrimitive",
    "version": "1.2.0"
}
